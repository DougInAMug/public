List of resources documenting issues with consensus

- [Consensus decision-making](https://en.wikipedia.org/wiki/Consensus_decision-making#Consensus_Voting_2) [Wikipedia]
- [Occupiers! Stop Using Consensus!](http://occupywallst.org/article/occupiers-stop-using-consensus/) 2013-02-13
- [Consensus is a means, not an end](http://beautifultrouble.org/principle/consensus-is-a-means-not-an-end/) 
- [Blocking progress: consensus decision making in the anti-nuclear movement](https://libcom.org/library/blocking-progress-consensus-decision-making-anti-nuclear-movement) 1983
- [Beyond ‘Consensus’ Decision-Making](http://nickdowson.net/2016/02/07/beyond-consensus-decision-making/) 2016-02-07
- [Against consensus, for dissensus](http://preorg.org/against-consensus-for-dissensus/) 2014-06-6
- [The Origins of Collective Decision Making (Synopsis)](http://lchc.ucsd.edu/MCA/Mail/xmcamail.2016-04.dir/pdfxI0mTmeH4l.pdf) 2016-04
- [Consenseless](http://www.occupylondonfilm.com/2018/03/27/consenseless/) 2018-03-27
- [The Theology of Consensus](http://berkeleyjournal.org/2015/05/the-theology-of-consensus/) 2015-03-26
- 
