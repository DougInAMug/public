# Title
blah

## Subtitle?
blah

## Metadata
blah

## Summary
All key info.

## Introduction
ind action is sometimes shit
coord
voting

vote and veto are palindromes
you can't veto the status quo

Why is this relevant to me? (the reader)
What is the context?

## Set-up
- Cooperation: interacton based on autonomy, equivalence & mutual interest
- Voting: a type of indirect interaction
- Cooperation re voting:
  - Autonomy: direct participation
  - Equivalence: equal participation
  - Mutual interest: acceptable over preferable
    - mutual interest vs self-interest
    - care for self and others
- Coop outcome: most acceptable, achievable






## Knock-down

## Example

## Conclusion

### References

### Conflict of Interests

### Acknowledgements

### Licensing


---

war of attrition = talk until we agree
birth lottery = how much of financial wealth is luck
starting a paragraph with however
thought: 5 year olds saying we were here first is proto-nationalism


---

<!-- 

# Meta

I want to write an article about why I think score voting with control options and acceptance orientation is good represents a significantly more cooperative approach to decision making than plurality, ranking, consenus or systemic consensus.

I want to write something that is mostly practical, but with reasonable philosophical and theoretical grounding: simply engaging with readers intuition would be adequate.

Some members of my imagined audience include: friends at Edinburgh Student Housing Cooperative, other people who want to copy our Constitution but want to understand our voting system, members of pretty much any social group which surpasses Dunbars limit.

you want to end up with a decision that people will do

-->

<!--
<label for="mn-demo" class="margin-toggle">&#8853;</label>
<input type="checkbox" id="mn-demo" class="margin-toggle"/>
<span class="marginnote">
  This is a margin note. Notice there isn’t a number preceding the note.
</span>
-->

<!--
here's some text and bang<label for="sn-demo" class="margin-toggle sidenote-number"></label><input type="checkbox" id="sn-demo" class="margin-toggle"/><span class="sidenote">a side note appeared.</span> But as I was saying...
-->

<!--
<blockquote>"it is the greatest happiness of the greatest number that is the measure of right and wrong"</blockquote> 

An elegant description of cooperation is the "art of pursuing the object of common desires in common."

Consider a group of people who are deciding whether to paint their bike-shed Blue, Red or Green. They vote by each choosing one option and selecting the most chosen option (i.e. a relative majority/plurality vote.) Blue wins by a narrow margin, an outcome quite unacceptable to the large minority which voted Green. However, Blue-voters and Green-voters find Red to be acceptable and both would have preferred it—despite it not being their first personal preference—since they share a mutual interest: that Red wasn't chosen demonstrates that the method was inadequate to respect mutual interest.

Some methods attempt to do this by allowing participants to veto proposals (e.g. unanimous consent or consensus) with the reasoning that no participant need endure a decision they find unacceptable. However, if the option to veto is viewed positively as the option to _change nothing_ then a single voter who prefers things the way they are is in a position of dictatorial power to select their preferred option regardless of how (un)acceptable the others find it.

The future is shaped by our present decisions in the same way the present has been shaped by our past decisions. 
-->

# memes
Keeping things the same is an option. Don't keep it as an implied default, list it as an explicit option.
for equality between voters, there must be equality between options
veto turns conservatives into dictators


<!DOCTYPE html>
<html>
  <head>
    <title>Cooperative Voting</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="tufte-hacked.css"/>
  </head>
  <body>
    <div id="content">
    
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.6/showdown.min.js"></script>
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML'></script> -->
    <script>
      var conv = new showdown.Converter();
      var txt = document.getElementById('content').innerHTML;
      console.log(txt);
      document.getElementById('content').innerHTML = conv.makeHtml(txt);
    </script>
  </body>
</html>




6
Cp. also the last note (70) to chapter 10.
Although my own position is, I believe, clearly enough implied in the text, I may perhaps briefly formulate what seems to me the most important principles of humanitarian and equalitarian ethics. Tolerance towards all who are not intolerant and who do not propagate intolerance. (For this exception, cp. what is said in notes 4 and 6 to chapter 7.) This implies, especially, that the moral derisions of others should be treated with respect, as long as such decisions do not conflict with the principle of tolerance. (2) The recognition that all moral urgency has its basis in the urgency of suffering or pain. It is, I believe, the greatest mistake of utilitarianism (and other forms of hedonism) that it does not recognize that from the moral point of view suffering and happiness must not be treated as symmetrical ; that is to say, the promotion of happiness is in any case much less urgent than the rendering of help to those who suffer, and the attempt to prevent suffering. (The latter task has little to do with matters of taste ', the former much.) Cp. also note 2 to chapter 9.

- Popper, K.R., 1945. The Open Society and Its Enemies, Volume 1, The Spell of Plato, George Routledge & Sons. p. 205, Notes to Chapter 5, 6. 

2
I believe that there is, from the ethical point of view, no symmetry between suffering and happiness, or between pain and pleasure. Both the
Utilitarians and Kant (* Promote other people's happiness.') seem to me (at least in their formulations) fundamentally wrong in this point, which is, however, not one for rational argument (for the irrational aspect of ethical beliefs, see note 1 1 to the present chapter, and chapter 24) In my opinion (cp. note 6 (2) to chapter 5) human suffering makes a direct moral appeal, namely, the appeal for help, while there is no similar call to increase the happiness of a man who is doing well anyway. (A further criticism of Utilitarianism would be that pain cannot be outweighed by pleasure, especially not one man's pain by another man's pleasure. and Instead of the 
greatest happiness of the greatest number, one should more modestly demand
and further, that unavoidable
the least amount of suffering for anybody
I find that there is
suffering should be distributed as equally as possible.)
some kind of analogy between this view of ethics and the view of scientific
methodology which I have advocated in my Logik der Forschung. Just as in
the field of ethics it is much clearer to formulate our demands negatively,
of
i.e., to demand the elimination of suffering rather than the promotion
;
.
.
;
.CHAPTER Q/NOTES 3-7
242
happiness, so it is clearer to formulate the task of scientific method as the
elimination of false theories (from the various theorie

- Popper, K.R., 1945. The Open Society and Its Enemies, Volume 1, The Spell of Plato, George Routledge & Sons. p. 241, Notes to Chapter 9, 2. 

title: Making cooperative decisions
short_description: A part-talk, part-workshop exploration into the philosophy, theory and practice of making cooperative decisions.
long_description: How about reaching collective decisions in a way that is simple, scalable and cooperative? Score voting provides a possibility distinct from the often utopian ideal of consensus, the needless polarization of majority voting or the paradoxical complexity of rank voting. I—Doug—have spent the last few years working with a variety of voluntary organizations in Germany during which the need for better decision-making methods became clear. This session is a distillation experiences with Systemisches Konsensieren ('Systemic Consensus,') discussions with The Center for Election Science and personal experimentation.
duration: 60-150 minutes
requirements: projector (VGA, displayPort or HDMI connection,) space for 10 people
preferences: space for 20+ people, able to prepare hot drinks, advertisement through your platform
date: between the 5th and 19th of July or between the 24th of July and the 2nd of August (both ranges inclusive)
cost: free. I am open to personal donations, donations to the space, not taking any donations, etc.

deciding
    chance
    leader
    voting
        unanimity
        plurality
        ordinal
        cardina
        
# Decision-making experiences
Methods
- clear hierarchies
- unclear hierarchies
- (uncoordinated)
- majority voting
- consensus
- systemisches konsensieren 

Issues
- simple decisions taking ages
- emotional decisions explode
- progressive ideas stifled
- confusion about who does the work

# Disclaimer
- I have a personal interest to make a cooperative network of cooperative groups
- I want to persuade you that
    - it's OK to separate discussion and decision
    - more than 2 options is good
    - veto is bad
    
## **vote (n.)**
"... _from Latin_ votum "**a vow**, _wish, promise to a god, solemn pledge, dedication,"_..."

voting: collective, formal decision-making process (e.g. majority, consensus, Borda count)

class: center, middle

# Cooperative decisions: what
The most acceptable, achievable option.


voting is not:
- chance
- a dictatorship
- a market
    
a voting system defines
- how people can express their opinions on a set of options
- how expressed opinions are used to select from those options
