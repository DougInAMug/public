# Cooperative Voting Systems
## —three features thereof

_v0.91 | 2018-04-07 | [Doug Webb](https://douginamug.gitlab.io/)_

<!--
CHANGELOG
v0.9: first intelligible document
v0.91: integrating Johns feedback
-->

---

## Summary
LASTTTTTTTTTTTTTTTTTTTTTTTTTt

---

## Introduction
People often get what they want by deciding for themselves and acting individually—but not always. Sometimes the combination of individual actions results in an outcome no one desires. This seemingly paradoxical situation can be observed in cases where users of a shared resource degrade it through short-term and unsustainable use. For example, if a group of shepherds each keep too many sheep, they will destroy their shared pasture through excessive grazing. Individual action also fails to deliver when a goal is too large or complex for anyone to achieve it on their own, for example creating a smartphone from raw materials. Such goals are only possible through the economy of scale and specialization that collective action allows. Acting collectively allows people to achieve desirable goals—and avoid undesirable outcomes—which wouldn't be possible individually.

Decision precedes action, and how multiple people decide is somewhat more complicated than for a single person. In groups which are small, similar and stable, collective decisions can be reached directly through informal meetings or conversations. The absence of formality is so advantageous to work-flow that affinity groups of activists and Scrum software teams intentionally limit their group size. However as groups get larger, more diverse or changeable, it becomes increasingly difficult to decide in this way due to restrictions in time, space and cognitive capacity. Hierarchical command is one alternative, but the chance that those at the top make decisions which are aligned with everyone else is slim and thus decisions made in this way typically require significant enforcement to succeed. Market force is another option, however the 'race to the bottom' phenomenon mirrors the first problem of individual action described previously, leading to the degradation of shared (human) resources.

Voting presents a direct way of reaching collective decisions. Voting methods are formal procedures which specify how participants can express their opinions on a set of options, and how the expressed opinions are used to select from the options. This approach has the possibility to reach decisions which are more aligned with the individuals involved, thus requiring less or no enforcement to succeed. The variables within a voting method systematically affect which decisions are made, and although voting always results in collective decisions, those decisions are not necessarily cooperative.

This article aims to provide practical advice for making voting more cooperative. First I define what I mean by cooperative voting and outline criteria for such voting methods, then I present three concrete features which fulfill my criteria and then I finish by outlining the features in an example.

## Definitions and Criteria ##
By cooperation I mean a quality of human interaction based on autonomy, equivalence and mutual-interest. Voting is a type of indirect interaction, the quality of which is restricted to the possibilities provided by a particular system: while no system can—by definition—make people cooperate, systems may have features which allow cooperation, or prevent it. In this section I outline a definition of cooperative voting systems by considering what it means for a decision-making method to allow autonomy, equivalence and mutual interest.

### Autonomy ###
Autonomy describes the basic freedom of an individual to act according to their interests, free from coercion. However, a voting system can't ensure that those participating are willing, nor that those affected are included. Hence autonomy in this context is limited to the whether it allows participants to directly and deterministically influence the decision: all voting systems fulfill this basic requirement of cooperation by definition. Counterexample decision-making methods include those based on random chance (e.g. lottery) or those in which some decide for others (e.g. dictatorships.)
 
### Equivalence ###
Equivalence refers to the equal valuation of people; that despite all the differences, people have the same fundamental worth as humans. A voting system exhibits equivalence if it provides each participant an equal opportunity to influence the decision, as summarized by the _one person, one vote_ principle. Some voting systems introduce inequality based on financial wealth, such as those which allocate voting power proportional to share-holding (i.e. in companies). Other voting systems introduce inequality based on personal preference: in cases where participants can veto proposals, participants which prefer the status quo can essentially select their preferred option, regardless of how (un)acceptable it is for everyone else.

### Mutual-interest ###
Mutual-interest occurs when people consider the preferences of others in addition to their own: mutual-interest leads to cooperation; self-interest leads to competition. Mutual-interest indicates a person's willingness to compromise in the face of differing opinions, especially the willingness to accept a less personally preferable option in the case another person finds their most prefered option to be unacceptable. The idea that there is a qualitative asymmetry between positive and negative preference is reflective of the belief that "human suffering makes a direct moral appeal, namely, the appeal for help, while there is no similar call to increase the happiness of a person who is doing well anyway" (Popper, 1945). This belief has the practical benefit in that decisions with smaller proportions negative preference are less likely to meet resistance and are therefore more likely to be succeed.

However, selecting options solely on the basis of minimizing negative preference is problematic. To do so would mean that an option which most people are strongly positive towards is outweighed by the slightly negative preference of a single person, and that an option which everyone is completely neutral towards is preferable for the group. The practical issue with this belief is a decision no one positively supports may not be realized in a cooperative group, where people choose to do things out of their intrinsic motivation. Combining the importance of having enough support and minimizing resistance, a voting system is said to allow mutual-interest if it selects the most acceptable, achievable outcome.

Cooperative voting systems are herein considered to be those which provide each participant an equal opportunity to directly and deterministically influence the decision in order to select the most acceptable, achievable outcome

## Features ##
The three major variables impacting a decision are the available options, how people can express their preferences and how the preferences are used to select from the options. In the previous section I argued that for a voting system to be cooperative it should allow voters to interact in with autonomy, equivalence and mutual interest. Voting systems allow autonomy by defintion, and equivalence is allowed simply by giving each voter equal power and disallowing veto: many familiar systems fulfill these criteria including majority voting. As such, the features proposed for each of the system variables is focussed mainly on allowing mutual interest in order to select the most acceptable, achievable outcome.

### Options: an acceptable one for everyone ###
At least two options are required to make a decision.
Of the two options, one arises from the fact that a decision is needed at all: some issue with the current way things are—the status quo—leads to a proposed alternative.
Another option is always to change nothing and maintain the status quo.
However, only having the options of a specific change or no change means that voters which detest the status quo may vote for the alternative even if they don't really like it, and vice-versa. 
Indeed, decisions are rarely binary; there is almost always more than one alternative in a given situation, and allowing multiple alternatives increases the chance that every voter has an option they are positive about. 
Unfortunately, even with multiple alternatives, only a fraction of the possible alternatives can be made available during a given vote: this means the optimal option for a group may be unavailable or worse, that a voter is forced to _vote for the lesser evil_.
One feature that ensures there is always an acceptable option for every voter is to include the option to vote again with new options.
This option allows people to express that they want some kind of alternative to the status quo that is not specified in any of the currently available options: it essentially represents all other options.
It could be called 'vote again', 'find other options' or 'none of the above' and must specify when the next vote will occur and how new options can be added.

![a diagram representing the expansive nature of the "find other options" option](https://gitlab.com/DougInAMug/public/raw/master/coopVoting/a_findOtherOptions.svg.png)


## Expression: getting the whole picture ##
A person expresses their preference with their vote.
Since votes must be combinable to reach a decision, people are essentially required to simplify their complex preferences into numerical expressions.
However, for a system to allow mutual-interest the method of expression must allow the transmission of sufficient information to identify the most acceptable, achievable option, 

The simplest method of expression is to let participants select their favorite option (i.e. majority voting.)
Unfortunately this method loses all information on non-favorite options, which makes it impossible to identify reasonable compromises.
This inability to identify acceptable options is highlighted by the fact that similar proposals divide the voters who support a common idea: the voters are unable to express that any of the similar proposals are acceptable.

Another method is to let voters rank options in order of preference (i.e. ordinal methods.) 
While this method does enable voters to indicate their relative preference of every option, it loses all information in terms of magnitude (how much) and polarity (positive/negative.)
For example, does someone actually like or dislike an option they ranked third out of five?
Additionally ranked methods can actively encourage voters to rank options falsely in order to secure a better personal outcome (Arrow, 1963), reducing the certainty that expressed preference reflects actual preference.

A third method is to let voters independently score options (i.e. cardinal methods.)
Although no voting system can be completely free from tactical voting when there are three or more options (Gibbard, 1973) score voting encourages fewest distortions, most notably it never encourages voters to falsify their order of preference.
The simplest form of score voting is to use a 2-point scoring range, often referred to as approval voting: this is like majority voting, except you can vote for every option you 'approve' of.
However a 2-point range without a neutral mid-point makes it impossible to distinguish between active positive or negative preference.
Using symmetrical ranges of positive and negative numbers with 3 points or more (e.g.: -2, -1, 0, +1, +2) allows voters to indicate the degree to which they find options (un)acceptable with negative scores, and the degree to which they support acceptable options with positive scores.
Although ranges with more points allow scoring which is more fine-grained, ranges with more than 11 points (i.e. -5 to +5) seems to bring diminishing returns due to extra cognitive load and simplified usage.
Hence a second feature for cooperative voting is to let participants score each option, preferably using a range of positive and negative numbers with at least 3 points but not more than 11. This enables voters to express themselves honestly without punishment and provides detailed information about option preferences.

![a graphical comparison of plurality, rank and score methods](https://gitlab.com/DougInAMug/public/raw/master/coopVoting/b_scoreEach.svg.png)

######################################

## Combination: acceptable, achievable outcomes
In the final stage of voting, votes are combined using a given system to determine the decision.

Different methods of vote processing can result in different outcomes from the same set of votes, and the degree to which a voting system is cooperative rests upon its ability to select the most acceptable, achievable outcome at this point.

In the previous section I argued for allowing voters to express themselves using independent scores.

In the case of score voting the outcome can be reached simply by calculating the average (mean) score for each option and selecting the one with the highest average.

Computer simulations of several common voting systems shows mean-based score voting to result in the least voter dissatisfaction ('Bayesian regret').

However, if a scoring range allows people to express positive or negative preferece, then calculating the mean essentially leads to positive scores canceling out negative ones (e.g. +3 + -3 = 0).

This equation of positive preference with negative is contrary to mutual-interest as outlined earlier, and hinders the selection of more acceptable outcomes.

A simple feature to select more acceptable outcomes is to multiply the weight of negative scores before totalling. 

For example, doubling negative scores would lead to mean scores which are influenced twice as much by negative preference.

However, using a multiplying negative scores too heavily invalidates positive preference. 

For example, if a factor is larger than the number of people who are voting (e.g. a factor of 100 in a group of 50) then a single negative score will outweigh all possible positive scores. 

This approach could lead to the selection of options with little to no support which are subsequently unlikely to succeed, or to voters voting tactically by only scoring on the negative side of the range (since the positive side has no impact.)

Hence a factor between 1 and the number of voters may be an appropriate starting point.

![a scale with a sad emoji weighing more than a happy one](https://gitlab.com/DougInAMug/public/raw/master/coopVoting/c_weightingFactor.svg.png)

## Example one: majority upgrade
A group makes holds regular meetings where they make decisions by raising their hands 'for' or 'against' a proposal (i.e. majority). They want to do things more cooperatively without sacrificing the speed or simplicity of their current method. First, they include the option to 'postpone' a decision to the next meeting and then they allow themselves to raise their hand for all the option they accept (i.e. approval voting.) The increase in complexity is minimal, and it only require 50 % more time to count 3 options instead of 2.

## Example two: migrating from veto
A group makes decisions using consensus: they have meetings in which they aim to the best, mutually-acceptable proposal via moderated discussion whihc they finally decide on by allowing people to block (veto) the proposal. They want to do things more cooperatively without compromizing their desire to ensure everyone can accept every decision. They change what it means to 'block' by allowing people to block the status quo or the proposal: if both options receive a block, the decision is automatically postponed to the next meeting.

## Example three: online
- NOTA, -5 to +5, *2

A group makes decisions online using score voting app. They want to do things more cooperatively without losing the

## Conclusion

I propose that cooperative voting—which respects individual autonomy, interpersonal equivalence and mutual interest—should feature: 1) the inclusion of an option to ‘Find other options’ by default, 2) the ability for participants to expressively score each option, and 3) the appropriate multiplication of negative scores before tallying. All these features are present in the free, open-source voting application [Ukuvota](https://ukuvota.world) and in the procedures to make formal decisions at [Kanthaus](https://kanthaus.online).

Organizations need not adopt all features at once, they can do so incrementally. From plurality voting one could first introduce 'find other options' and allow participants to 'vote for each proposal they approve of' (a.k.a. Approval Voting.) From veto methods, one could first switch to score voting with an _infinite_ negative multiplication factor. 

In groups of very large size or diversity (e.g. countries) mutual interest may be reduced to the point where a significant portion of participants vote tactically, and cooperation as descibed above is not a reasonable goal. In these cases simple score voting without the multiplication of negative scores, or  score voting hybrids such as '[STAR](http://www.equal.vote/starvoting)' (Score Then Automatic Runoff) or [Quadratic Voting](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2343956) may lead to better outcomes. 

While long-term and wide-reaching decisions are better done formally, perhaps the majority of decisions made in organizations are informal, allowing an organization to be adaptable at the point of operation. How informal decisions are made can be positively influenced by promoting certain norms such as the [Advice Process](http://www.reinventingorganizationswiki.com/Decision_Making), [3-pirate-rule](https://wiki.pirateparty.be/PP_Structure_Proposal#Three_Pirate_Rule) or [doocracy](https://communitywiki.org/wiki/DoOcracy).

I wish cooperative groups well in their endeavors and hope that they are able to radically challenge their fundaments while remaining functional.

---

### References
- Arrow, K.J., 1963. Social choice and individual values. 2nd ed., Wiley, New York
- Gibbard, A., 1973.  Manipulation  of  voting  schemes:  a  general  result.  Econometrica  41,  587–601.
- Ord, T., 201x. Why I'm not a negative Utilitarian. http://www.amirrorclear.net/academic/ideas/negative-utilitarianism/
- Omelas?
- that university guy
- http://rangevoting.org/sen-recsys2011.pdf
- Warren D. Smith, 2000. http://rangevoting.org/WarrenSmithPages/homepage/rangevote.pdf

### Conflict of interests
I really want to live in a more cooperative world.

### Acknowledgements
I first thank Erich Visotschnig and Siegfried Schrotta for developing the decision-making framework [Systemisches Konsensieren](http://www.sk-prinzip.eu/#) (Systemic Consensus), Joachim Thome for introducing it to me, and the early [yunity](https://yunity.org/en) group for trying it out: this framework was the primary influence for this text, indeed the first two features are almost identical. To the enthusiastic discussions of the [Centre for Election Science](https://www.electology.org/) community which helped me clarify my issues with the ignorance of positive opinion within Systemisches Konsensieren. Being hosted free-of-charge by [Projektwerkstaat Saasen](http://www.projektwerkstatt.de/pwerk/saasen.html), [Kanthaus](https://kanthaus.online/en) and many other places made it possible for me gift forward this work. Many thanks to Wolfi Silverdream for his development of Ukuvota which motivated me to finish this work. To the countless others, countless thanks.

### Licensing
Please copy and use! All original text and figures licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) Attribution, feedback and donations are all welcome.

Please used
Use as you please
No rights reserved
xyz nice, not necessary

[Twemoji2](https://github.com/twitter/twemoji/tree/gh-pages/2/svg) by Twitter, Inc. which is licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).



