# Critique on §7. Application Procedure (Constitution)

_The following is copy/pasted from the working Constitution at https://kanthaus.online/en/governance/constitution_

1. The applicant must not be present during the procedure.
1. Firstly participants should take a preliminary vote in which they anonymously express whether they ‘support’ (+), ‘accept’ (0) or ‘oppose' (-) the application succeeding.
1. Secondly participants should discuss the application and, in particular, raise and address any concerns.
1. Finally participants should take a binding vote in the same fashion as the preliminary vote. This time—
	1. if there are 3 or more times as many ‘support’ votes to ‘oppose’ votes, the application succeeds, or
	1. else the application fails at that time.
1. The applicant should promptly be informed whether their application has succeeded or failed, but not the breakdown of which votes were cast.

---

## Doug's reasoning of the Application procedure
### paraphrased according to Joachim
The voting method is supposed to be conservative, meaning it should be able to stabilize the existing group while and by giving an emotionally supportive environment and a voting system that reflects that.

The voting system in the evaluation rounds provides a biased framework that protects minorities from people that they dont want to be with by giving their resistance more weight then supportive voices towards new people.

The protection against misuse of this system should work by people addressing when someone might misuse it and being able to exclude them if they should misuse the system. Unreasoned or malicious or emotionally impulsiveness voting might qualify as misuse.

### phrased by Doug
The application procedure consists of—
1. an open-format meeting between a prospective member and the existing group followed by,
1. a meeting of just the existing members initiated with an exercise to combat groupthink and anchoring followed by,
1. a vote in which those opposing the prospective member have a threefold weight over those supporting.

One of three Kanthaus principles is "self-determination": voluntary groups must coordinate through cooperation, by definition. There are always differences between people which is generally a good thing, but in order to cooperate these differences must rest within a window of tolerable diversity. This window shrinks the closer people are to each other and the more time they spend together: it is at its smallest when individuals live together in the same space.

The founders of a group have 100 % diversity tolerance, since they all voluntary supported/accepted each other. People applying to become members later may be opposed by some of the existing members. Continuing at 100 % diversity tolerance would require consensus-with-veto: diversification/growth too slow, stagnation.

Score voting which treats options equally could lead to the selection of an option with a diversity tolerance as low as 51%. For example, 49% oppose an a new individual but are overruled: diversification/growth too fast, fragmentation.

This reasoning lead to selecting a binary choice between the requested position and reevaluation at a lower position with a 3:1 ratio of support:oppose. This leads to the lowest diversity tolerance for a new member being 76%: diversification/growth 'just right', progress.

---

## Joachim's attack, paraphrased according to Doug [corrections by Joachim in brackets]
### Summary
The current procedure allows a minority (>25%) to reject any applicant [and members] regardless of all other opinion. This favors irrational, emotionally impulsive and/or intolerant characteristics will lead to the group becoming progressively dysfunctional and eventually collapsing: project failure.

### Reasoning
Fact 1: All decisions in the procedure are binary choices between progressing one Position up or being reevaluated at the current Position. For example, a Visitor applying to become a Volunteer would become a Volunteer if successful or be reevaluated as a Visitor if unsuccussful: a Visitor being reevluated as a Visitor would remain as a Visitor if successful or be recommended to leave if unsuccessful.

Fact 2: One person voting 'oppose' counteracts three people voting 'support' (and an infinite number of people voting 'accept',) therefore a minority (>25%) is able to reject any applicant.

Fact 3: All individuals are eventually reevaluated according to the same procedure.

Assumption 1: Applicants with dysfunctional characteristics [irresponsibility, irrational behaviour] are often more ~~charismatic~~ [not more charismatic,they are more self-victimizing and by that triggering the help of more agreeable people > social neotony] —and therefore less likely to be opposed—than applicants with desirable characteristics.

Assumption 2: Voters with desirable characteristics are less likely to oppose applicants than voters with dysfunctional characteristics.

[Assumption 3: The mechanic of the voting system in general gives more power to resistance over enthusiasm in direct comparison. This will lead to competitive and disagreeabe behaviour, if used consequently, or, if used covertly, to pseudoharmony withh rising levels of unresolved conflicts until explosive conflict or implosive failure]

Thus a problem arises for a group of individuals with desirable characteristics **if**—
  - they accept individuals with dys. char.s to >25% of the group membership, and
  - voters with dys. char.s oppose applicants with des. char.s with greater than 1/3 the frequency that voters with des. char.s oppose applicants with dys. char.s.

**Then** a runaway scenario occurs in which the group becomes progressively populated by individuals with dysfunctional characteristics.

This problem is accelerated since existing Volunteers and Members are reevaluated.

### Proposed solutions  
The first easy fix would be to not use a binary vote for the application and reevaluation procedure but instead rate all options in both directions simultainiously. For a Visitor, options would be:
- a) advances to Volunteer 
- b) stays as Visitor 
- c) has to leave the project  

All options can be voted on with support, acceptance and resistance. That would quickly solve the problem of a minority deciding over a majority.

The next recommended fix would be to either seperate pro and contravotes from each other (quasi syscon) or at least put resistance and support in symmetry, which would still nourish competitiveness but would at least not lead to overpowering disagreeableness

To be more conservative in the sense of a more stable group would be to have a longer evaluation phase for guests and clearer commitments from volunteers and members. Hint: Conservativeness and flexibilty normally exist in an antagonistic relation

---








